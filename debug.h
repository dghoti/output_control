//***************************************************
//To enable debugging in code, use macros as below
//add line below to code and uncomment/comment as needed
//#define DEBUG
//***************************************************

#ifdef DEBUG
 #include "Arduino.h"
 #define DEBUG_BEGIN(x)    Serial.begin(x)
 #define DEBUG_PRINT(x)    Serial.print (x)
 #define DEBUG_PRINTDEC(x) Serial.print (x, DEC)
 #define DEBUG_PRINTln(x)  Serial.println (x)
#else
 #define DEBUG_BEGIN(x)
 #define DEBUG_PRINT(x)
 #define DEBUG_PRINTDEC(x)
 #define DEBUG_PRINTln(x)
#endif
