#include "output_control.h"
#define DEBUG     //comment out to remove debug output
#include "debug.h"

//****************************************************************
// relay setup
// ESP2866 Pins to use for relay control:
// D0, D1, D2, D3, D4, D5, D6, D7, D8, 10(SD3),

outputControl relay[10] = { // set up array of relays
  outputControl(D0),
  outputControl(D1),
  outputControl(D2),
  outputControl(D3),
  outputControl(D4),
  outputControl(D5),
  outputControl(D6),
  outputControl(D7),
  outputControl(D8),
  outputControl(10),

};
void setup() {
  Serial.begin(9600);
  Serial.println("test");
  // put your setup code here, to run once:
  for (int x = 0; x < 10; x++)
  {
    relay[x].begin();
  }
}

void loop() {
  // put your main code here, to run repeatedly:
   for (int x = 0; x < 10; x++)
    {
      relay[x].toggle();
      delay(5);
    }
}
