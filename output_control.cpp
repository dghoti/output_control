#include "Arduino.h"
#include "output_control.h"


// set pin to use and initial state to off, phase to "off":-
outputControl::outputControl(byte pin)
{
  _outputpin=pin;
  _outputstate=LOW;
  _offPhase=false;
}
// set pin to use and initial state, phase to "off":-
outputControl::outputControl(byte pin, bool initialState)
{
  _outputpin=pin;
  _outputstate=initialState;
  _offPhase=false;
}
//set pin to use and initial state, declaring the phase as "on"/"off":-
outputControl(byte pin, bool initialState, bool initialPhase)
{
  _outputpin=pin;
  _outputstate=initialState;
  _offPhase=initialPhase;
}
void outputControl::begin() // set pin to output and set initial state
{
  digitalWrite(_outputpin, _outputstate);
  #ifdef DEBUG
  Serial.print(_outputpin);
  Serial.print(" - Started, Phase: ");
  if (_outputstate==_offPhase){
    Serial.print(_outputpin);
  } else {
    Serial.print(!_outputpin);
  }
  #endif
  pinMode(_outputpin, OUTPUT);
}

bool outputControl::toggle() // switch state and return new phase after updating output
{
  _outputstate=!_outputstate;
  digitalWrite(_outputpin,_outputstate);
  return ((!_outputstate&&_offPhase)||(_outputstate&&!_offPhase)); // return current phase (1-on, 0-off)
}

bool outputControl::readState() // just return current state
{
  return _outputstate;
}

bool outputControl::readPhase() // return 1 for "on", 0 for "off"
{
  return return ((!_outputstate&&_offPhase)||(_outputstate&&!_offPhase)); // return current phase (1-on, 0-off)
}

void outputControl::setState(boolean req_state) //set output to requested state
{
  _outputstate=req_state;
  digitalWrite(_outputpin,_outputstate);
}

void outputControl::setPhase(boolean req_phase) //set output to requested phase
{
  if (_offPhase==LOW){ //If "OFF"=LOW
  if (req_phase==0) { //req=OFF
    _outputstate=LOW;
  } else { //req=ON
    _outputstate=HIGH;
  }
} else { //if "OFF" = HIGH
if (req_phase==0) {//req=OFF
  _outputstate=HIGH;
} else { //req=ON
  _outputstate=LOW;
}
}
_outputstate=req_state;
digitalWrite(_outputpin,_outputstate);
}

void outputControl::on() //set to "on" phase
{
  _outputstate=!_offPhase;
  digitalWrite(_outputpin,_outputstate);
}

void outputControl::off() //set to "off" phase
{
  _outputstate=_offPhase;
  digitalWrite(_outputpin,_outputstate);
}
