#ifndef output_control
#define output_control

#include "Arduino.h"

/**************************************************
* Library for using Arduino/ESP2866 pins as outputs.
* Allows for simpler syntax, and choice of LOW/HIGH
* as "ON"/"OFF", without needing to remember actual pin state
* (C) 2017 Dee Power d(dot)ghoti(at)gmail(dot)com
* Free to use, no warranties given.
* Beard stuff and meat always welcome!
***************************************************/

// "phase" is used to determine if pin is "ON" or "OFF"
// regardless of actual state (LOW/HIGH).
// This makes code easier to follow, especially in mixed
// configurations.

class outputControl
{
public:
  //Choice of constructors:
  // set pin to use and initial state to off, phase to "off":-
  outputControl(byte pin);
  // set pin to use and initial state, phase to "off":-
  outputControl(byte pin, bool initialState);
  //set pin to use and initial state, declaring the phase as "on"/"off":-
  outputControl(byte pin, bool initialState, bool initialPhase);

  void begin(); // set pin to output and set initial state
  bool toggle(); // switch state and return new phase after updating output
  bool readState(); // just return current state (HIGH/LOW) of pin
  bool readPhase(); // return 1 for "on", 0 for "off"
  void setState(boolean req_state); //set output to requested state
  void setPhase(boolean req_phase); //set output to requested phase
  void on(); //set to "on" phase
  void off(); //set to "off" phase
private:
  byte _outputpin; // which pin output is connected to
  bool _outputstate; //state of output
  bool _offPhase; //which output state is "off"
};

#endif
